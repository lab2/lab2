package edu.ucsd.cs110w.temperature;
public class Celsius extends Temperature 
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		String ret = Float.toString(getValue()) + " C";
		return ret;
	}
	@Override
	public Temperature toCelsius() {
		Temperature retVal = new Celsius(getValue());
		return retVal;
	}
	@Override
	public Temperature toFahrenheit() {
		float val = getValue();
		float conversion = (val*9.0f)/5.0f + 32;
		Temperature retVal = new Fahrenheit(conversion);
		return retVal;
		
	}
	@Override
	public Temperature toKelvin() {
		float val = getValue();
		float conversion = val + 273.15f;
		Temperature retVal = new Kelvin(conversion);
		return retVal;
	}
}
